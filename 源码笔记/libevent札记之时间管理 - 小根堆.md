[TOC]
## 最大堆和最小堆 的定义
类型名称： 最小堆（MinHeap）
数据对象集： 完全二叉树，每个结点的元素值不大于其子结点的元素值
最大堆同理可得

是不是堆要注意从根结点到任意结点路径上结点序列的有序性！
另外 ，关于堆，为了保证数组第一个元素就是heap的根节点， heap基本上都是用数组(或者其他的连续存储空间)作为其存储结构的。

## 最小堆的操作
### 堆的定义 

```c++
//堆的定义
typedef struct min_heap
{
    struct event** p; // p指向了一个动态分配的数组
    unsigned n, a;
} min_heap_t;
```
关于p,由realloc分配，malloc分配的内存是位于堆，realloc则对malloc申请的内存进行大小的调整  
p数组元素为event*，n表示目前保存了多少个元素，a表示p指向的内存的尺寸。

其中结构体event定义了min_heap_idx
```c++
struct event {
    ...
    int min_heap_idx;   /* for managing timeouts */
    ...
}
```

### 典型小根堆与libevent中的差别
- 插入元素为例，来对比说明，下面伪代码中的size 表示当前堆的元素个数  
典型的伪代码逻辑如下：
```c
Heap[size++] <-  new; //先放到数组末尾，元素个数+1
//下面就是 shift_up()的代码逻辑，不断的将 new 向上调整
_child = size;
while(_child>0) // 循环
{
    _parent <- (_child-1)/2; // 计算 parent
    if(Heap[_parent].key <  Heap[_child].key)
        break; // 调整结束，跳出循环
    swap(_parent, _child); // 交换 parent 和 child
}
```
- libevent中的处理类似于选择排序算法中的作法，先得到插入值的相应位置，再进行交换  
这样子实际上每次调整做少了一次赋值操作  

![libevent的插入](http://git.oschina.net/feemic/blog/raw/master/img/heap.png)

伪代码如下
```c
// 下面就是 shift_up()的代码逻辑，不断的将 new 的“预留位置”向上调整
_hole = size; // _hole 就是为 new 预留的位置，但并不立刻将 new 放上
while(_hole>0) // 循环
{
    _parent <- (_hole-1)/2; // 计算 parent
    if(Heap[_parent].key < new.key)
    break; // 调整结束，跳出循环
    Heap[_hole] = Heap[_parent]; // 将 parent 向下调整
    _hole = _parent; // 将_hole 调整到_parent
}
Heap[_hole] = new; // 调整结束，将 new 插入到_hole 指示的位置
size++; // 元素个数+1
```


### 代码实例 - 最小堆的操作 之插入
知道了Libevent中与典型最小堆的不同外，以上面的插入为例补充Libevent中最小堆的代码操作   
检查调整内存空间大小 --> 调用shift_up_插入  
```c++
static inline int            min_heap_push(min_heap_t* s, struct event* e);
int min_heap_push(min_heap_t* s, struct event* e)
{
    if(min_heap_reserve(s, s->n + 1))  //调整内存空间大小,元素 n+1
        return -1;
    min_heap_shift_up_(s, s->n++, e);
    return 0;
}


```
 

libevent中的 min_heap_shift_up_  
从上调整，其中，s->p[index]->min_heap_idx 为event结构体的min_heap_idx
```c++
void min_heap_shift_up_(min_heap_t* s, unsigned hole_index, struct event* e)
{
    unsigned parent = (hole_index - 1) / 2;  //父结点
    while(hole_index && min_heap_elem_greater(s->p[parent], e)) //比父结点小
    {
        (s->p[hole_index] = s->p[parent])->min_heap_idx = hole_index; 
        hole_index = parent;    //hole_index的位置
        parent = (hole_index - 1) / 2;  //赋值
    }
    (s->p[hole_index] = e)->min_heap_idx = hole_index;
}
```


### 代码实例 - 最小堆的操作 之取出
大部分时候，从堆里取元素只局限于取根节点。取完后，需要重新调整树使其依然为一个heap。  
![dd](http://www.cppblog.com/images/cppblog_com/kevinlynx/WindowsLiveWriter/libeventmin_heap_E003/heap_remove.jpg)  

```c++
struct event* min_heap_pop(min_heap_t* s)
{
    if(s->n)
    {
        struct event* e = *s->p;
        min_heap_shift_down_(s, 0u, s->p[--s->n]);
        e->min_heap_idx = -1;
        return e;
    }
    return 0;
}
```

min_heap_shift_down_ 函数进行向下调整  
```c++
/* hole_index 为取出的元素的位置，e为最右最下的元素值 */
void min_heap_shift_down_(min_heap_t* s, unsigned hole_index, struct event* e)
{
    /* 取得hole_index的右孩子节点索引 */
    unsigned min_child = 2 * (hole_index + 1);
    while(min_child <= s->n)
    {
        /* 有点恶心的一个表达式，目的就是取两个孩子节点中较大的那个孩子索引 */
        min_child -= min_child == s->n || min_heap_elem_greater(s->p[min_child], s->p[min_child - 1]);
        /* 找到了位置，这里似乎是个优化技巧，不知道具体原理 */
        if(!(min_heap_elem_greater(e, s->p[min_child])))
            break;
        /* 换位置 */
        (s->p[hole_index] = s->p[min_child])->min_heap_idx = hole_index;
        /* 重复这个过程 */
        hole_index = min_child;
        min_child = 2 * (hole_index + 1);
    }
    /* 执行第二步过程，将最右最下的节点插到空缺处 */
    min_heap_shift_up_(s, hole_index,  e);
} 
```


## 附：小根堆源代码 

```c++
//file : minheap-internal.h
#ifndef _MIN_HEAP_H_
#define _MIN_HEAP_H_

#include "event2/event.h"
#include "event2/event_struct.h"
#include "event2/util.h"

//堆的定义
typedef struct min_heap
{
    struct event** p;
    unsigned n, a;
} min_heap_t;
//static inline 内联函数，C++中的宏定义，提高函数调用效率
static inline void           min_heap_ctor(min_heap_t* s);
static inline void           min_heap_dtor(min_heap_t* s);
static inline void           min_heap_elem_init(struct event* e);
static inline int            min_heap_elem_greater(struct event *a, struct event *b);
static inline int            min_heap_empty(min_heap_t* s);
static inline unsigned       min_heap_size(min_heap_t* s);
static inline struct event*  min_heap_top(min_heap_t* s);
static inline int            min_heap_reserve(min_heap_t* s, unsigned n); //调整内存空间大小
static inline int            min_heap_push(min_heap_t* s, struct event* e); //进堆
static inline struct event*  min_heap_pop(min_heap_t* s); //堆首
static inline int            min_heap_erase(min_heap_t* s, struct event* e);
static inline void           min_heap_shift_up_(min_heap_t* s, unsigned hole_index, struct event* e); //向上调整
static inline void           min_heap_shift_down_(min_heap_t* s, unsigned hole_index, struct event* e); //向下调整

int min_heap_elem_greater(struct event *a, struct event *b)
{
    return evutil_timercmp(&a->ev_timeout, &b->ev_timeout, >);
}

void min_heap_ctor(min_heap_t* s) { s->p = 0; s->n = 0; s->a = 0; } //创建
void min_heap_dtor(min_heap_t* s) { free(s->p); } //删除
void min_heap_elem_init(struct event* e) { e->min_heap_idx = -1; }
int min_heap_empty(min_heap_t* s) { return 0u == s->n; }
unsigned min_heap_size(min_heap_t* s) { return s->n; }
struct event* min_heap_top(min_heap_t* s) { return s->n ? *s->p : 0; }

int min_heap_push(min_heap_t* s, struct event* e)
{
    if(min_heap_reserve(s, s->n + 1))
        return -1;
    min_heap_shift_up_(s, s->n++, e);
    return 0;
}

struct event* min_heap_pop(min_heap_t* s)
{
    if(s->n)
    {
        struct event* e = *s->p;
        min_heap_shift_down_(s, 0u, s->p[--s->n]);
        e->min_heap_idx = -1;
        return e;
    }
    return 0;
}

int min_heap_erase(min_heap_t* s, struct event* e)
{
    if(((unsigned int)-1) != e->min_heap_idx)
    {
        min_heap_shift_down_(s, e->min_heap_idx, s->p[--s->n]);
        e->min_heap_idx = -1;
        return 0;
    }
    return -1;
}

int min_heap_reserve(min_heap_t* s, unsigned n)
{
    if(s->a < n)
    {
        struct event** p;
        unsigned a = s->a ? s->a * 2 : 8;
        if(a < n)
            a = n;
        if(!(p = (struct event**)realloc(s->p, a * sizeof *p)))
            return -1;
        s->p = p;
        s->a = a;
    }
    return 0;
}

void min_heap_shift_up_(min_heap_t* s, unsigned hole_index, struct event* e)
{
    unsigned parent = (hole_index - 1) / 2;
    while(hole_index && min_heap_elem_greater(s->p[parent], e))
    {
        (s->p[hole_index] = s->p[parent])->min_heap_idx = hole_index;
        hole_index = parent;
        parent = (hole_index - 1) / 2;
    }
    (s->p[hole_index] = e)->min_heap_idx = hole_index;
}

void min_heap_shift_down_(min_heap_t* s, unsigned hole_index, struct event* e)
{
    unsigned min_child = 2 * (hole_index + 1);
    while(min_child <= s->n)
    {
        min_child -= min_child == s->n || min_heap_elem_greater(s->p[min_child], s->p[min_child - 1]);
        if(!(min_heap_elem_greater(e, s->p[min_child])))
            break;
        (s->p[hole_index] = s->p[min_child])->min_heap_idx = hole_index;
        hole_index = min_child;
        min_child = 2 * (hole_index + 1);
    }
    min_heap_shift_up_(s, hole_index,  e);
}

#endif
```

参考文章：  
libevent源码解析  
<http://www.cppblog.com/kevinlynx/archive/2008/07/18/56511.html>