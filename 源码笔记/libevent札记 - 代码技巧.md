[TOC]  
> 阅读了Libevent(version:libevent-2.0.1-alpha)的源码，为了随时能翻阅，这里讲下阅读过程中注意到的代码技巧  
> 

###1.多平台代码
size_t  用size_t来表示最大可表示的整形，在32位，及64位保证不可溢出  
```c++
typedef unsigned int size_t; (Win32)
typedef unsigned __int64 size_t; (Win64)
```

###2.宏定义
在libevent中基本随处可见宏定义，下面总结一些源码阅读分析中遇到的源定义  
####2.1 时间减运算
```c++
#define evutil_timersub(tvp, uvp, vvp)                  \
    do {                                \
        (vvp)->tv_sec = (tvp)->tv_sec - (uvp)->tv_sec;      \
        (vvp)->tv_usec = (tvp)->tv_usec - (uvp)->tv_usec;   \
        if ((vvp)->tv_usec < 0) {               \
            (vvp)->tv_sec--;                \
            (vvp)->tv_usec += 1000000;          \
        }                           \
    } while (0)
```

####2.2 可扩展性，时间大小比较   
```c++
if (evutil_timercmp(tv, &base->event_tv, >=))
#define evutil_timercmp(tvp, uvp, cmp)                  \
    (((tvp)->tv_sec == (uvp)->tv_sec) ?             \
     ((tvp)->tv_usec cmp (uvp)->tv_usec) :              \
     ((tvp)->tv_sec cmp (uvp)->tv_sec))
```


####2.3 判断条件中的宏定义
如判断EVBASE是否有锁
```c++
if (EVBASE_USING_LOCKS(base))
...
/** True if the given event_base is set up to use locking */
#define EVBASE_USING_LOCKS(base)            \
    (base != NULL && (base)->th_base_lock != NULL)
```

###3.兼容性
####3.1 C与C++
```c++
#ifdef __cplusplus
extern "C" {
#endif
```
