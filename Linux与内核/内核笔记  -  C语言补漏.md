[TOC]  

### typedef 函数指针
```c++
int inc(int a)
{
　return(++a);
}
int multi(int*a,int*b,int*c)
{
　return(*c=*a**b);
}
typedef int(FUNC1)(int);
typedef int(FUNC2) (int*,int*,int*);
 
void show(FUNC2 fun,int arg1, int*arg2)
{
　FUNC1 * p=&inc;
　int temp =p(arg1);
　fun(&temp,&arg1, arg2);
　printf("%d\n",*arg2);
}
 
int main( )
{
　int a;
　show(multi,10,&a);
　return 0;
}
```

show(multi,10,&a)
形参对应被调函数：  
```c++
void show(FUNC2 fun,int arg1, int* arg2)  
= void show ( int fun(int*, int*, int*) , int arg1, int* arg2)  
= void show ( int multi(int*, int*, int*) , int arg1, int* arg2)   
{
FUNC1 * p=&inc;        // int* p = &inc, p指向inc函数的地址
int temp =p(arg1);     //int temp = p(10) = inc(10) = 11;
fun(&temp,&arg1, arg2);//fun(&11, &10, &a) = multi(&11, &10, &a),得a = 11*10 = 110;
printf("%d\n",*arg2);  //结果是110
}
```

### 全局变量与局部变量   
1.程序的局部变量存在于 栈 中，全局变量存在于 全局存储区 中  
2.可以用引用头文件的方式，或用extern关键字，可以引用一个已经定义过的全局变量  
3.在不同的C文件中以static形式来声明同名全局变量。可以在不同的C文件中声明同名的全局变量，前提是其中只能有一个C文件中对此变量赋初值，此时连接不会出错  
4.static全局变量与普通的全局变量有什么区别？static局部变量和普通局部变量有什么区别？static函数与普通函数有什么区别？  
静态全局变量则限制了其作用域， 即只在定义该变量的源文件内有效。static全局变量只初使化一次，防止在其他文件单元中被引用;  
静态局部变量改变了它的生存期，static局部变量只被初始化一次，下一次依据上一次结果值；  
static函数在内存中只有一份，普通函数在每个被调用中维持一份拷贝  


### 随便放的const   
#### const修饰指针变量  
```c++
int main()
{
   int i=10;
   int j=1;
   const int *p1;//(1)
   int const *p2=&i; //(2)
   p2=&j;//(3)
   int *const p3=&i;//(4)
   *p3=20;//(5)
   *p2=30;//(6)
   p3=&j;//(7)
    return 0;
}
```

如上，遇到const修饰的时候，从右向左读     
`const int *p1`     const修饰整块内容，内容不能变；但是p1本身可修改   
`int const *p2=&i`    与p1相同    
`int *const p3=&i`    const修饰p3，指针不能变；但可以修改其内容  

故`*p3=20`是可以的，`*p2=30` 是错误的，`p3=&j `也是不对的  


### 指针被玩坏  

#### 1.指针的步伐   
```c++
int main()
{
   int a[5]={1,2,3,4,5};
   int *p=(int *)(&a+1);
   printf("%d",*(p-1));
}
```
&a的步伐为5，p的步伐为1  
若将上面改为`int *p = (int *)(a + 1);  `,得出的结果是1，因为此时a代表第一个元素的地址，的步伐为1  

#### 2.指针的指针  
指针其实就是一个变量，它保存的值被视为地址。因为指针类型可以合法地使用“*”运算符， 做提领运算。将变量的值视为一个地址， 然后从这个地址中读取值   
```c++
int main(void)
{
   short *p1 = 0;
   int **p2 = 0;
   ++p1;
   ++p2;
   printf("p1 = %d, p2 = %d\n", p1, p2);
   return 0;
}
```
short *p1 = 0 与  short *p1 = NULL 其实是等同的  
p1是一个short类型地址，步伐为2字节，所以p1++，其实地址增加了2  
p2指向地址，32位系统中，步伐为4字节，所以p2++，其实地址增加了4  
所以答案为 2，8  
扩展：  
`short *p1 = (short*)(3);`  结果为5
`int **p2 = (int*)(3);`  结果为11  

####3.指针与数组   
```c
int a[2][3];  
printf("size of pointer step is 0x%X\n", sizeof(*(&a[0][0])));  
```

```c
&a[0][0] address is 0x39F9E0
&a[0][0]+1 address is 0x39F9E4
size of pointer step is 0x4

&a[0] address is 0x39F9E0
&a[0]+1 address is 0x39F9EC
size of pointer step is 0xC

a address is 0x39F9E0
a+1 address is 0x39F9EC
size of pointer step is 0xC

&a address is 0x39F9E0
&a+1 address is 0x39F9F8
size of pointer step is 0x18
```

>·&a[0][0]的类型是int*pointer， 所以步长为4字节。  
·&a[0]的类型为int（*pointer） [3]， 所以步长为12字节。  
·a的类型也为int（*pointer） [3]， 所以其步长也为12字节。  
·&a的类型为int（*pointer） [2][3]， 所以其步长为24字节  

####4.指针数组与数组指针
当我们定义一个数组时，一般用类似于`int a[10]`的模式来定义  
当我们定义一个指针时，一般用类似于`int *p`的模式来定义   
所以，类似的，当定义如下时，`int *p[10]` => `(int *) p[10]` 是一个指针类型，所以，表示的是十个指针放在数组里，即指针数组  
当定义`int (*p)[10]`时，是一个整型数组，p指针指向了这个数组，即p是指针   

更进一步的变法 - 函数指针：  
int (*a)(int);//函数指针，指向有一个参数并且返回类型 均为int的函数  
int (*a[10])(int); //函数指针的数组，指向有一个参数并且返回类型均为int的函数的数组  
例题： 声明一个指向含有10个元素的数组的指针，其中每个元素是一个函数指针，该函数的返回值是int，参数是int*   
答案：`int (*(*p)[10])(int *)`    
一个指向含有10个元素的数组的指针，由上面可知，`int (*p)[10]`    
左边还有星号，则`int* (*p)[10]`,数组存放的为指针类型    
函数指针`int (*a)(int)` ，a是一个函数指针，故由此可得最终的答案  


### 结构体对齐的事  
1.为什么不能用memcmp比较结构体  

```c++
typedef struct padding_type {
   short m1;
   int m2;
} padding_type_t;
int main()
{
   padding_type_t a = {
      a.m1 = 0,
      a.m2 = 0,
   };
   padding_type_t b;
   memset(&b, 0, sizeof(b));
   if (0 == memcmp(&a, &b, sizeof(a))) {
      printf("Equal!\n");
   }
   else{
      printf("No equal!\n");
   }
   return 0;
}
```

由于对齐问题，编译器会在m1后面插入两个padding字节， 而这两个
字节的内容却是“随机”的。   结构体b由于调用了memset对整个结构体占用的内存进行了清零， 其padding
的值自然就为0。   


2.pragma   

```c++
#pragma pack(2)
struct node {

   char f;
   int a;
   short b;

};
#pragma pop(2)
```
pragma 表示  对齐系数  
不加pragma pack的话，得出的结构体大小是12，加了之后，得出的是8   
可见，如果pragma pack(1)的话，大小就是7了  

### 不容易注意到的细节  
#### switch语句括号的参数   
switch语句后的控制表达式只能是short、char、int、long整数类型和枚举类型，不能是float，double类型  
switch其实是一个比对，short，char,int,long等都是可以比对的   

switch如果匹配到了的话，如果后面没有break，会不管条件一直匹配下去   
如果从一开始没有匹配到的话，则不会输出  

### 神奇整数类型转换
```c++
int main(void)
{
   signed int a = -1;
   unsigned int b = 2;
   signed short c = -1;
   unsigned short d = 2;
   PRINT_COMPARE_RESULT(a, b);
   PRINT_COMPARE_RESULT(c, d);
   return 0;
}
```

得出的结果为  
```
a > b
c < d
```
**C标准规定， 当进行整数提升时， 如果int类型可以表示原始类型的所有值时， 它就被转换为int类型； 不然则被转换为可以表示更多数的unsigned int**   
由于这个标准规定，所以出现了上面的两种情况  

当signed int和unsigned int进行比较时， signed int会被转换为unsigned int。   -1的值即0xFFFFFFFF， 就被视为无符号整数的最大值， 因此a>b。  
所以当c和d进行比较时， c和d的类型分别是short和unsigned short，那么它们就会被转换为int类型， 则实际是对（int） -1和（int） 2进行比较， 结果自然是c＜d。  


### 浮点陷阱

####　“x==x”也能为假？  
除了使用宏定义， 或者用高级语言中的操作符重载之类的。但如果说要求使用最原始的C语言表达式， 那么什么时候“x==x”会是假呢？    
看下例：  
```c++
float x = 0xffffffff; 
int a = 0xffffffff;
memcpy(&x, &a, sizeof(x));//cpy a to x
if (x == x) {
   printf("Equal\n");
}
else{
   printf("Not equal\n");
}
```

结果为Not equal ，调试发现，经memcpy后，x的值变为-nan  
由于浮点数的存储格式与整数不同，所以，float 转化为int的时候，memcpy将0xff填充到x的地址时，这时保证了x储存的一定是0xffffffff， 但很可惜它不是一个
合法的浮点值， 而是一个特殊值NaN。  
当使用memcpy将0xff填充到x的地址时， 这时保证了x储存的一定是0xffffffff， 但很可惜它不是一个合法的浮点值， 而是一个特殊值NaN。  
作为一个非法的浮点数NaN， 当它与任何数值相比较时， 都会返回假。   所以就有了比较意外的结果x==x为假， x即不大于0， 不小于0， 也不等于0。   

####  浮点数的精度限制   
IEEE 754标准， float类型是1个sign bit、 8个exponent bits和23个mantissa bits。 而double类型是1个sign bit、 11个exponent bits和52个mantissa bits。   

一般要使用一个范围来确定x是否为0。  
单精度数尾数23位，加上默认的小数点前的1位1，2^(23+1) = 16777216。  
10^7 < 16777216 < 10^8，单精度浮点数的有效位数是7-8位  
双精度的尾数52位存储，2^(52+1) = 9007199254740992，那么有10^16 < 9007199254740992 < 10^17，所以双精度的有效位数是16-17位。  

所以要判断一个单精度浮点数：则是if( abs(f) <= 1e-6)；  
要判断一个双精度浮点数：则是if( abs(f) <= 1e-15 )；  

#### 两个特殊的浮点值  -- Nan与infinite  
```c++
float x = 1/0.0   
x = 0/0.0;
```
当1除以0.0时， 得到的是**infinite**， 而用0除以0.0时， 得到的就是**NaN**。   
scanf不仅接受inf和nan的输入， 并将其视为浮点数的两种特殊值。  
所以遇到浮点值时，要首先判断其是否为合法的浮点值  
C库提供了两个库函数isinf和isnan， 分别用于判断浮点数是否为infinite和NaN   


