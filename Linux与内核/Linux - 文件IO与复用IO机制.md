[TOC]  
## 1.文件共享  
### 1.1 如果两个独立进程各自打开了同一文件  
![](http://img.blog.csdn.net/20140407101516312?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbHZ5aWxvbmczMTY=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)  

- 打开此文件的每个进程都得到一个文件对象，但只有一个v节点表项。  
- 这种安排使每个进程都有它自己对该文件的当前位移量。   
- 这种情况不会增加对应的打开文件引用计数，而会增加dentry的引用。
- file对象有引用计数，记录了引用这个对象的文件描述符个数，只有当引用计数为0时，内核才销毁file对象  

- 用leek定位到当前文件尾端，在向文件写入（write）与使用O_APPEND打开（open）相比，前者是非原子操作，非线程安全

### 1.2 软链接与硬链接的区别  
软链接（符号链接） ln -s   source  target  
硬链接 （实体链接）ln       source  target  

- 不允许给目录创建硬链接
- 硬链接共用一个inode号，说明是同一个文件，而软链接拥有不同的inode号 
- 软链接包含有原文件的路径信息，当原文件移到其他目录中，再访问链接文件，系统就找不到了 
![|65%](http://git.oschina.net/feemic/blog/raw/master/img/file_io.jpg)   

从这里可以看出，硬链接有个计数，同时当`ll`命令时，只有软链接才有l位，说明软链接是“真正意思上的链接”，是一种文件类型，依靠于源文件存在   

## 2.IO复用    
### 2.1 select函数  
select：  
`int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);`    
1) 第一个参数nfds的值必须大于集合中的最大socket的值（windows下例外）；     
2) select返回后timeout的值可能被改写，这是一个依赖实现的特性，请保证程序逻辑不受该特性影响；   
3) fd_set中存放的必须是有效的socket描述符，否则会导致select提前返回；     
4) 必须分别处理正常、被信号中断、超时及出错的情况；   
5) 不通过返回值判断具体有多少个描述符及三个集合中一共有多少个状态被侦测，应通过遍历三个集合进行获取；    
6) Linux平台禁止使用select监测事件，请用poll/epoll代替。如果select不用于监测事件，只用于睡眠，可例外。   
原因：select在文件描述符的值大等于1024时，可能发生内存越界访问的错误，poll/epoll能够实现select具备的功能，所以推荐使用poll/epoll代替select。    

因为每次调用时都会对连接进行**线性遍历**，所以随着FD的增加会造成遍历速度慢的“线性下降性能问题”。   

使用例子： 
```c 
#include "unp.h"
for ( ; ; ) {
    FD_SET(fileno(fp),&ret);
    FD_SET(sockfd,&ret);
    maxfdp1 = max(fileno(fp),sockfd) + 1;
    Select(maxfd+1, &rset, NULL, NULL, NULL);  
    if(FD_ISSET(sockfd,&ret)){
    }
    if(FD_ISSET(fileno(fp),&ret)){
    }
}
```


### 2.2 poll 的实现  
poll本质上和select没有区别，但是它没有最大连接数的限制，原因是它是基于链表来存储的   

### 2.3 epoll的实现    
#### 1.概述
使用例子  
epoll操作过程需要三个接口，分别如下：   
```c++
#include <sys/epoll.h>
int epoll_create(int size); //创建一个描述符
int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);//事件注册函数
int epoll_wait(int epfd, struct epoll_event * events, int maxevents, int timeout);//等待事件的产生
```


提到epoll，就要提到select，select有几大缺点：  
1. 每次调用select，都需要把fd集合**从用户态拷贝到内核态**  
2. 同时每次调用select都需要在**内核遍历**传递进来的所有fd  
3. select支持的**文件描述符数量**太小了，默认是1024    

epoll 相比select，多了**边缘触发**，**句柄数目不受限**，不会随着FD的数目增长而降低效率，**mmap加速**内核与用户空间的消息传递  

1.边缘触发  
**水平触发：** 不处理下次会再持续通知   
**边缘触发：** 对fd不处理下次不会再提醒，效果高   

2.异步，句柄数目不受限，不是轮询的方式，不会随着FD数目的增加效率下降。只有活跃可用的FD才会调用callback函数；  

#### 2.从epoll源码看epoll的实现   

```c
SYSCALL_DEFINE1(epoll_create1, int, flags)
{
    int error, fd;
    struct eventpoll *ep = NULL; //重要的结构体eventpoll  
    struct file *file;  //文件结构指针，指向epoll文件
    error = ep_alloc(&ep);//分配一个eventpoll的结构体，并设置相应的初始值
    if (error < 0)
        return error;
    //创建一个名叫[eventpoll]的文件，并返回其文件结构指针,这个文件代表着epoll实例
    error = anon_inode_getfd("[eventpoll]", &eventpoll_fops, ep,
                 O_RDWR | (flags & O_CLOEXEC));
    if (error < 0)
        ep_free(ep);
    return error;
}
```

从上面epoll_create的实际入口函数可以看到，epoll的整体框架  
我们需要关注的是其中的数据结构及如何实现异步与mmap  
**2.1 eventpoll 结构体**  
```c
struct eventpoll {
    spinlock_t lock;
    struct mutex mtx;  
    wait_queue_head_t wq; /* 调用epoll_wait()的等待队列*/
    wait_queue_head_t poll_wait;
    struct list_head rdllist; /* 所有已经ready的epitem都在这个链表 */
    struct rb_root rbr;/* 所有要监听的epitem都在这里，红黑树 */
    struct user_struct *user;/*这里保存了一些用户变量,*/
};
```
eventpoll结构体是epoll的核心里面存放着许多信息，主要包括   
1.`struct rb_root rbr ` 添加事件，封装成struct    epitem结构体，加到红黑树的相应节点上   
2.`struct list_head rdllist `这是一个双向链表,这个双向链表中存放的是就绪的事件当我们调用epoll_wait的时候这些事件会返回给用户   
3.互斥锁 - 涉及到添加, 修改或者删除监听fd的时候, 以及epoll_wait返回, 向用户空间传递数据时都会持有互斥锁，可多个线程中同时执行epoll相关的操作  

**2.2 mmap的影子**  
当epoll_create1执行完毕，我们就创建了一个代表epoll实例的文件并返回其文件描述符，但是我们要怎么找到eventpoll这个最重要的结构体呢，答案在上面代码中的anon_inode_getfd函数,我们将eventpoll的结构体变量ep作为函数的第3个参数传了进去，
下面是函数anon_inode_getfd的部分代码，删掉了不相关部分   

```c
struct file *anon_inode_getfd(const char *name,
                const struct file_operations *fops,
                void *priv, int flags)
{
    struct file *file;
    file = alloc_file(&path, OPEN_FMODE(flags), fops);
    file->f_mapping = anon_inode_inode->i_mapping;
    file->f_flags = flags & (O_ACCMODE | O_NONBLOCK);
    file->private_data = priv;
    return file;
}
```
这里是创建一个匿名fd, epollfd本身并不存在一个真正的文件与之对应,所以内核需要创建一个"虚拟"的文件, 并为之分配真正的struct file结构, 而且有真正的fd.   

这里2个参数比较关键:
eventpoll_fops, fops就是file operations, 就是当你对这个文件(这里是虚拟的)进行操作(比如读)时,fops里面的函数指针指向真正的操作实现  
epoll只实现了poll和release(就是close)操作, 其它文件系统操作都有VFS全权处理了.
ep, ep就是struct epollevent, 它会作为一个私有数据保存在struct file的private指针里面.
其实说白了, 就是为了能通过fd找到struct file, 通过struct file能找到eventpoll结构.


**2.3 回调函数 **   
当我们监听的fd发生状态改变时, 它会被调用.参数key被当作一个 unsigned long 整数使用, 携带的是events.
```c
static int ep_poll_callback(wait_queue_t *wait, unsigned mode, int sync, void *key)
{
    int pwake = 0;
    unsigned long flags;
    struct epitem *epi = ep_item_from_wait(wait);//从等待队列获取epitem.需要知道哪个进程挂载到这个设备
    struct eventpoll *ep = epi->ep;//获取
    spin_lock_irqsave(&ep->lock, flags);

    if (!(epi->event.events & ~EP_PRIVATE_BITS))
        goto out_unlock;

    if (key && !((unsigned long) key & epi->event.events))
        goto out_unlock;
    /* 可能已经在循环获取events的情况的处理 
     */
    if (unlikely(ep->ovflist != EP_UNACTIVE_PTR)) {
        if (epi->next == EP_UNACTIVE_PTR) {
            epi->next = ep->ovflist;
            ep->ovflist = epi;
        }
        goto out_unlock;
    }
    /* 将当前的epitem放入ready list */
    if (!ep_is_linked(&epi->rdllink))
        list_add_tail(&epi->rdllink, &ep->rdllist);
    /* 唤醒epoll_wait... */
    if (waitqueue_active(&ep->wq))
        wake_up_locked(&ep->wq);
    /* 如果epollfd也在被poll, 那就唤醒队列里面的所有成员. */
    if (waitqueue_active(&ep->poll_wait))
        pwake++;
out_unlock:
    spin_unlock_irqrestore(&ep->lock, flags);
    if (pwake)
        ep_poll_safewake(&ep->poll_wait);
    return 1;
}
```



### 优缺点总结   
1、表面上看epoll的性能最好，但是在连接数少并且连接都十分活跃的情况下，select和poll的性能可能比epoll好，毕竟epoll的通知机制需要很多函数回调。  
2、select低效是因为每次它都需要轮询。但低效也是相对的，视情况而定，也可通过良好的设计改善  

