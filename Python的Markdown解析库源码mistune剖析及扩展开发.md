[TOC]  
>由于在做文本分析的时候，需要对Markdown文本进行解析，为了满足相应功能开发，读了解析库Mistune并依此进行了扩展开发。  
(02/28 更新，之前时间匆忙，内容存在些许错误，在此更正)   

##前言
Mistune是Python平台上解析markdown的插件。测试了几个解析库如Markdown，Misaka等的性能及扩展，决定选用Mistune。同时由于Mistune与Misaka有CPython版本，后续也会进行相应的记录。    
## 了解库相应的概念    
要对markdown文本进行解析，我们首先得知道markdown文本到底有什么样的格式，依照不同的文本格式可以采取不同的策略。  
对于markdown格式而言，可以大概的分为大致两类    
一类是block块级元素，可以看成是单独存在的，即“非闭合”的,如  

>标题 ## 或 引用块 >  或 列表块 . 等    
  
而另一类inline内联元素，与block区别是成对存在的，即“闭合的”,常见的有  

>链接 \[]() 或 字体加粗 \*\* text \*\*, 代码块 \```c++ \```等   
  
像源码中，视为block级别的有   
```
'newline', 'hrule', 'block_code', 'fences', 'heading','nptable', 'lheading', 'block_quote' ... 
```
inline级别的有  
```
['escape', 'inline_html', 'autolink', 'url','footnote', 'link', 'reflink',    
'nolink','double_emphasis', 'emphasis', 'code','linebreak', 'strikethrough',   
'text',]  
```
当然，除此之外，可能还存在注脚，尽管脚注不是标准 MarkDown 的范畴。也有可能存在公式及流程图，这里暂且不谈。    
```
Markdown [^1] 脚注可能是这样的。
[^1]: 脚注1
```
所以，可以看到比较清楚的思路，如果是自己做一个Markdown解析库的话，那么可以先对文本进行正则匹配并分类，并生成相应格式的网页格式文本出来。  
匹配分类的过程可以称为称为词法分析(lexical analysis)，相应的词法分析器称为lexer。语法或规则经过lexer处理后输出一系列的token，再对token进行文本插入或替换，就能得到最终的网页文本了。  
在Mistune中，有两个这样的lexer而言，根据inline和block分为下面两类    
```python
self.inline = InlineLexer(renderer, **kwargs)  
self.block = block or BlockLexer(BlockGrammar())
```
下面便从实例出发，看看两个lexer的具体处理流程吧。  

## 从怎么使用入手 
准备一段markdown文本，使用如下  
```python
md_file = "[TOC]> \n#A\n##AB\n##AC\n##AD\n#B\n#C\n#D\n![ds](/ipc.jpg)hello[^hello]\n[^hello]: hi\nhello[^2llo]\n[^2llo]: hi"
markdown = mistune.Markdown(my_renderer)
return markdown(md_file)
```

这里可以看出，是调用了Markdown这个类，同时对自定义my_renderer进行传参赋值，返回一个实例后，就可以直接用了  
那如何自定义了，引用官网的比较简单的自定义，添加自己的规则   
```python
class WikiLinkRenderer(Renderer):
    def wiki_link(self, alt, link):
        return '<a href="%s">%s</a>' % (link, alt)

class WikiLinkInlineLexer(InlineLexer):
    def enable_wiki_link(self):
        # add wiki_link rules
        self.rules.wiki_link = re.compile(
            r'\[\['                   # [[
            r'([\s\S]+?\|[\s\S]+?)'   # Page 2|Page 2
            r'\]\](?!\])'             # ]]
        )
        self.default_rules.insert(3, 'wiki_link')
    def output_wiki_link(self, m):
        text = m.group(1)
        alt, link = text.split('|')
        return self.renderer.wiki_link(alt, link)
```
这里的函数名要严格按照要求来   
可以看到，这里添加一条规则就是添加一条正则表达式，当添加一条规则时， `self.default_rules.insert(3, 'wiki_link')` 是向规则列表的某个地方插入相应的规则  
而生成的内容则是相应的想要转换的内容，这里容易让人想到，Mistune可以做的事是当规则匹配时，然后进行相应的替换，那Mistune是否是这么直接做的呢，先一步一步开始代码分析    

## 如何初始化   
于是开始查看其源代码，果不其然，进入Markdown之后，对其进行相应的初始化    
```python
class Markdown(object):
    def __init__(self, renderer=None, inline=None, block=None, **kwargs):
        if not renderer:
            renderer = Renderer(**kwargs)
        else:
            kwargs.update(renderer.options)

        self.renderer = renderer

        if inline and inspect.isclass(inline):
            inline = inline(renderer, **kwargs)
        if block and inspect.isclass(block):
            block = block(**kwargs)

        if inline:
            self.inline = inline
        else:
            self.inline = InlineLexer(renderer, **kwargs)

        self.block = block or BlockLexer(BlockGrammar())
        self.footnotes = []
        self.tokens = []

        # detect if it should parse text in block html
        self._parse_block_html = kwargs.get('parse_block_html')

    def __call__(self, text):
        return self.parse(text)

    def parse(self, text):
        out = self.output(preprocessing(text))
```
这里的类Markdown继承自object，是新类，可以看到，类里面有一个__call__方法，
当我们执行markdown(source_file)，则会调用__call__方法，从而执行parse函数    

## 开始解析  
看一下parse函数  
```    
def parse(self, text):
    out = self.output(preprocessing(text))
     keys = self.block.def_footnotes
    # reset block
    self.block.def_links = {}
    self.block.def_footnotes = {}
    # reset inline
    self.inline.links = {}
    self.inline.footnotes = {}
    if not self.footnotes:
        return out
    ......
```
可以看到，函数又调用output函数，调用output之前，对text进行了预处理，这里主要对一些unicode字符进行处理，我们直接看output函数   
output函数主要会调用 block和reverse
```python
    def output(self, text, rules=None):
        self.tokens = self.block(text, rules)
        self.tokens.reverse()
```
直接往下看，最终会调用BlockLexer类里的parse方法，这个函数才是真正地开始解析了，
```python
def parse(self, text, rules=None):
    text = text.rstrip('\n')

    if not rules:
        rules = self.default_rules

    def manipulate(text):
        for key in rules:
            rule = getattr(self.rules, key)
            m = rule.match(text)
            if not m:
                continue
            getattr(self, 'parse_%s' % key)(m)
            return m
        return False  # pragma: no cover

    while text:
        m = manipulate(text)
        if m is not False:
            text = text[len(m.group(0)):]
            continue
        if text:  # pragma: no cover
            raise RuntimeError('Infinite loop at: %s' % text)
    return self.tokens
```
从函数可以看出，parse进行一些预处理后，便开始对markdown文件text进行循环处理。在while循环里，manipulate函数的行为是这样了。从已有的转换规则里遍历，看是否与当前的text匹配，假如匹配，则调用相应的规则解析函数`getattr(self, 'parse_%s' % key)(m)`对其进行处理。同时返回时，对text进行截取，匹配到的长度直接丢弃，对后续进行匹配，如此循环  
需要注意的是，这个函数真正的目的是得到tokens。于是在调用相应的解析函数时，tokens的值会进行相应的填充  
以其中一个为例：
```
def parse_block_quote(self, m):
    self.tokens.append({'type': 'block_quote_start'})
    # clean leading >
    cap = _block_quote_leading_pattern.sub('', m.group(0))
    self.parse(cap)
    self.tokens.append({'type': 'block_quote_end'})
```

经过分析也知道了，当添加进一个规则进去后，相应的函数必须严格地遵循其命名规则 ，以便在这个环节进行处理。最后经过这里的parse函数处理返回的是下面的这个形式  

```python
#self.token
[{'type': 'block_quote_end'}, {'type': 'footnote_end', 'key': '2llo'}, {'text': 'hi', 'type': 'paragraph'}, {'type': 'footnote_start', 'key': '2llo'}, {'text': 'hello[^2llo]', 'type': 'paragraph'}, 
{'type': 'footnote_end', 'key': 'hello'}, {'text': 'hi', 'type': 'paragraph'}, {'type': 'footnote_start', 'key': 'hello'}, {'text': '![ds](/ipc.jpg)hello[^hello]', 'type': 'paragraph'},
{'text': 'D', 'type': 'heading', 'level': 1}, {'text': 'C', 'type': 'heading', 'level': 1}, {'text': 'B', 'type': 'heading', 'level': 1}, {'text': 'AD', 'type': 'heading', 'level': 2},
{'text': 'AC', 'type': 'heading', 'level': 2}, {'text': 'AB', 'type': 'heading', 'level': 2}, {'text': 'A', 'type': 'heading', 'level': 1}, {'type': 'block_quote_start'}]
```

呃，这里Mistune对其先进行了分段处理，而不是直接替换，这里基本可以看到转换化文件的雏形了  

## 对tokens进行处理   
到了这里，就需要对tokens这个已经生成好的列表进行处理就行了  
在相应的函数里  
```python
def output(self, text, rules=None):
    self.tokens = self.block(text, rules)
    self.tokens.reverse()

    self.inline.setup(self.block.def_links, self.block.def_footnotes)
    out = self.renderer.placeholder()
    while self.pop():
        out += self.tok()
    return out
```

可以看到，进行block处理后，还对列表进行了反转，为什么要进行反转呢？因为接下来要对列表里的尾部进行pop操作，而尾部必须是最开始的字符，这样了才能按顺序拼好字符串  
下面的语句`out = self.renderer.placeholder()`返回一个placeholder空字符串给out,out就是最后要输出的内容  
这里对tokens进行处理
```
def pop(self):
    if not self.tokens:
        return None
    self.token = self.tokens.pop()
    return self.token
```
```
def tok(self):
    t = self.token['type']
    # sepcial cases
    if t.endswith('_start'):
        t = t[:-6]
    return getattr(self, 'output_%s' % t)()
```
可以看到，对相应的类型对应预处理后，又调用了相应的规则函数  
以标题为例：  
```
def output_heading(self):
    return self.renderer.header(
        self.inline(self.token['text']),
        self.token['level'],
        self.token['text'],
    )
```

```
def header(self, text, level, raw=None):
    return '<h%d>%s</h%d>\n' % (level, text, level)
```

至此，便解析完毕了  
以一个例子总结一下其中的主要解析流程    
```python
def link_out(link, text):
    return '<a href="%s">%s</a>' % (text, link)

def keyify(key):
    key_pattern = re.compile(r'\s+')
    return key_pattern.sub(' ', key.lower())
def parse_link(m):
    token = {}
    key = keyify(m.group(1))
    token[key] = {
        'link': m.group(2),
    }
    return token

def_links =re.compile(r'\[(.*?)\]\((.*?)\)')
text = "[dscri](www.feemic.com)"
m = def_links.match(text)
if m:
    print m.groups()
    print parse_link(m)
    print link_out(m.group(1),m.group(2))
```

## 基于源库开发 - Markdown文章生成概述  - 简单入门  
基于上面的理解，我们可以对相应的类进行处理，生成想要的文章概述    
这里，对上面的tokens进行处理便行，检测到tokens相应的内容，再进行提取    
```python
class DescriMarkdown(mistune.Markdown):
    '''
    a class inherite and modify a Markdown class
    parse the markdown to decription words
    '''
    def reset_descri(self):
        self.text_len = 0

    def iter_description(self, max_len=200):
        for token in self.tokens:
            if 'text' in token and self.text_len < max_len:
                text = token['text']
                self.text_len += len(text)
                yield text

    def output_description(self):
        return self.decript_str

    def output(self, text, rules=None):
        self.tokens = self.block(text, rules)
        self.decript_str = ''.join(self.iter_description())
        return self.decript_str
```
通过解读源码可以对源库的框架了解得比较清楚，再度开发也比较方便   
这里，要加载reset_descri()记得对参数进行初始化    
```python
"""just out_put decription words"""
def md_description(source_file, default_len=150):
    pattern = "[TOC]"
    if source_file.startswith(pattern)：
        source_file = source_file[len(pattern):] 
    markdown = DescriMarkdown()
    markdown.reset_descri()
    return markdown(source_file)[:default_len]
```

到这里，基本了解了大概，但Mistune还有一些细节还涉及到，这留待后续处理  
## 基于源库开发 - markdown文章生成纯文本 - 进一步开发   
可以看到， 在上面的扩展开发，由于只是重写了解析的前面部分，所以尽管可以达到概述的生成，但还是有一些格式未能完全转换，于是对其作进一步的开发。  
这次，我们重写token生成后的部分代码，使得其能完全匹配到概述的所有格式   
留待更新  

## 进阶 - 剔除多余代码，完成相应的解析库   
留待更新  

## 总结   
可以看到，Mistune的整体流程还是比较清晰，可见，清晰的思路是代码简洁美观的前提。由于是第一次写源码分析笔记，难免会有一些疏漏，有发现相应纰漏还敬请指出  
>注：转载请注明出处 www.feemic.cn   

