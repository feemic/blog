[TOC]
### 1.TCP 端口
服务端熟知端口号：  
取值范围：0 ~ 1023。  
         
|应用程序    |FTP |TELNET |SMTP | DNS | TFTP |  HTTP |  HTTPS | SNMP|
| --------   | -- | ----- | --- | --- | ---- | ----- |  ----- | --- |
|熟知端口号  | 21 | 23    |25   |53   |69    |80     |443     |161  |  
     
客户端使用的端口号  
取值范围：49152 ~ 65535  

---

###2.报文结构  
TCP 报文段首部的前20个字节是固定的，后面有 4N 字节是根据需要而增加的。  
下图是把 TCP 报文中的首部放大来看。  
![](http://om6ayrafu.bkt.clouddn.com/post/understand-tcp-udp/CFC6314E4B2FD039C450821D946E93E2.png)

####确认号 Acknowledgemt Number  
占 4 个字节。表示期望收到对方下一个报文段的序号值。  
例如，通讯的一方收到了第一个 25kb 的报文，该报文的 序号值=0，那么就需要回复一个确认报文，其中的确认号 = 25600.  
####数据偏移 Offset
占 0.5 个字节 (4 位)。  
这个字段实际上是指出了 TCP 报文段的首部长度 ，它指出了 TCP报文段的数据起始处 距离 TCP报文的起始处 有多远。  
（注意 数据起始处 和 报文起始处 的意思）  
一个数据偏移量 = 4 byte，由于 4 位二进制数能表示的最大十进制数字是 15，因此数据偏移的最大值是 60 byte，这也侧面限制了 TCP 首部的最大长度。
####确认 ACK (Acknowlegemt)
当 ACK = 1 的时候，确认号（Acknowledgemt Number）有效。    
一般称携带 ACK 标志的 TCP 报文段为「确认报文段」。  
TCP 规定，在连接建立后所有传送的报文段都必须把 ACK 设置为 1。  
####同步 SYN (SYNchronization)
当 SYN = 1 的时候，表明这是一个请求连接报文段。  
一般称携带 SYN 标志的 TCP 报文段为「同步报文段」。  
在 TCP 三次握手中的第一个报文就是同步报文段，在连接建立时用来同步序号。  
对方若同意建立连接，则应在响应的报文段中使 SYN = 1 和 ACK = 1。  
####窗口大小 Window Size
占 2 字节。
该字段明确指出了现在允许对方发送的数据量，它告诉对方本端的 TCP 接收缓冲区还能容纳多少字节的数据，这样对方就可以控制发送数据的速度。
窗口大小的值是指，从本报文段首部中的确认号算起，接收方目前允许对方发送的数据量。  
例如，假如确认号是 701 ，窗口字段是 1000。这就表明，从 701号算起，发送此报文段的一方还有接收 1000 （字节序号是 701 ~ 1700）   个字节的数据的接收缓存空间。  

--- 

###3.TCP的连接
![](http://om6ayrafu.bkt.clouddn.com/post/understand-tcp-udp/08EAF7F3E7FFCEF3E781385BF62BA2BC.png)  
####3.1 三次握手  
第一次握手：   
客户端，SYN = 1，初始化一个序号 Sequence Number = J  

第二次握手：  
服务器收到了SYN报文，SYN=1，ACK=1，确认号 Ack Number=J+1，Sequence Number = K  

第三次握手：  
这时首部的 SYN 不再置为 1，而 ACK = 1，确认号 Acknowledgemt Number = K + 1，Sequence Number = J + 1。  
对于建立连接的三次握手，主要目的是初始化序号 Sequence   Number，并且通信的双方都需要告知对方自己的初始化序号。  
这个序号要作为以后的数据通信的序号，以保证应用层接收到的数据不会因为网络上的传输问题而乱序，因为TCP 会用这个序号来拼接数据。  

**TCP Flood 攻击**  
属于 DDOS 攻击的一种，让服务器一直等待回复，直到崩溃  
####3.2 四次挥手   
第一次挥手：  
客户端发送 FIN = 1，Sequence Number = M    

第二次挥手：    
服务端收到后，发ACK = 1， Sequence Number = M + 1  

第三次挥手：  
同时服务端发送结束报文段 FIN = 1，Sequence Number = N，然后进入 LAST_ACK 状态。

第四次挥手：  
客户端收到后发确认报文段ACK = 1, Sequence Number = N + 1，进入 TIME_WAIT 状态  
经过2MSL 之后，自动进入 CLOSED 状态  
服务端收到该报文之后，进入 CLOSED 状态。  
关于 TIME_WAIT 过渡到 CLOSED 状态说明：
从 TIME_WAIT 进入 CLOSED 需要经过 2MSL，其中 MSL 就叫做 最长报文段寿命（Maxinum Segment Lifetime），根据 RFC 793 建议该值这是为 2 分钟，也就是说需要经过 4 分钟，才进入 CLOSED 状态。

###4.TCP可靠传输的实现
通过 TCP 连接传输的数据，无差错、不丢失、不重复、并且按序到达。   
这就需要了解 TCP 的几种技术：  
> * 流量控制与滑动窗口    
> * 超时重传  
> * 拥塞控制


####4.1流量控制(滑动窗口协议)

流量控制就是让发送方的发送速率不要太快，让接收方来得及接受。利用滑动窗口机制可以很方便的在TCP连接上实现对发送方的流量控制。  
![](http://images.cnitblog.com/blog/153130/201308/12214258-07499554110c4fb08795049dc787c598.png)

因为 TCP 协议是全双工的，会话的双方都可以同时接收和发送，那么就需要各自维护一个「发送窗口」和「接收窗口」
(窗口即TCP报头里的窗口大小)  
滑动窗口本质上是描述**接受方的TCP数据报缓冲区大小**的数据，发送方根据这个数据来计算自己最多能发送多长的数据。  
如果发送方收到接受方的窗口大小为0的TCP数据报，那么发送方将停止发送数据，等到接受方发送窗口大小不为0的数据报的到来。  
TCP就是用这个窗口，慢慢的从数据的左边移动到右边，把处于窗口范围内的数据发送出去。这就是窗口的意义。  
窗口的大小是可以通过socket来制定的，4096并不是最理想的窗口大小，而16384则可以使吞吐量大大的增加。  

另外接收端相对于发送端还有不同的一点，**只有前面所有的段都确认的情况下才会移动左边界**，
在前面还有字节未接收但收到后面字节的情况下，窗口不会移动，并不对后续字节确认，以此确保对端会对这些数据重传。  
![](http://om6ayrafu.bkt.clouddn.com/post/understand-tcp-udp/686E3FC14C2DEF657C61ECBC16C9C954.png)


####4.2超时重传
RTO ( Retransmission Time-Out ) 重传超时时间  
RTT ( Round Trip Time ) 连接往返时间  
TCP 通过测量来获得连接当前 RTT 的一个估计值，并以该RTT估计值为基准来设置当前的 RTO。  
这就引入了一类算法的称呼：自适应重传算法（Adaptive Restransmission Algorithm） 
**Jacobson / Karels 算法**
>SRTT = SRTT + α ( RTT – SRTT ) —— 计算平滑 RTT  
>DevRTT = ( 1-β ) DevRTT + β ( | RTT - SRTT | ) ——计算平滑 RTT和真实的差距（加权移动平均）  
>RTO= µ SRTT + ∂ DevRTT  

其中：  
- α、β、μ、∂ 是可以调整的参数，在 RFC6298 中给出了对应的参考值，而在Linux下，α = 0.125，β = 0.25， μ = 1，∂ = 4；
*式子的理解*：  
- 第一条式子可以化为：SRTT = (1-α)SRTT + RTT   
式子类似于一阶滤波法，不过在这里，当α=较小时，权重倾向于上一次的值，即旧的RTT值，RTT会每次叠加，意义在于考虑进RTT的影响    
- 第二条式子关键在于 β (|RTT-SRTT|)，根据每次测量的 RTT 和旧的 SRTT 值进行运算，得出新的 DevRTT  
- 第三条式子比较放在了DevRTT  

**重传报文判定**  
发送一个报文段，设定的重传时间到了，还没有收到确认。于是重传报文段，经过一段时间后：收到了确认报文段。  
现在的问题是：如何判定此报文段是对先发送的报文段的确认，还是对后来重传的报文段的确认？？？  
![](http://img.blog.csdn.net/20170805220728932?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTcxMjE1MDE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
解决方案：  
Karn提出了一个算法：在计算加权平均RTT时，只要报文段重传了，就不采用其往返时间样本。这样得出的加权平均RTT和RTO就相对比较准确了。  
但是，但是，要是出现这样的情况呢？？：  
报文段的时延突然增大了很多。因此在原来得出的重传时间内，不会收到确认报文段。于是就重传报文段。但根据Karn算法，不考虑重传的报文段的往返时间样本。这样：超时重传时间就无法更新。  
因此：要对Karn算法进行修正：方法是：报文段每重传一次，就把超时冲传时间RTO增大一些。典型的做法是：取新的重传时间为2倍的旧的重传时间。当不再发生报文段的重传时，才根据上面给出公式计算超时重传时间。。 

####4.3拥塞控制  
慢启动与快重传  
**慢启动**  
指数增加到ssthresh --> 加法增大直到网络拥塞 --> 乘法减小 --> 更新ssthresh为拥塞时窗口的一半 --> 从1继续慢开始  
（当发送端收到连续三个重复的确认时，就执行“乘法减小”算法）   

![](http://img.blog.csdn.net/20170805220709551?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTcxMjE1MDE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)   

**快重传**   
可以看出慢启动后发生拥塞就一下子降到了1，窗口有点小，可以改进  
拥塞窗口 cwnd 现在不设置为 1，而是设置为慢开始门限 ssthresh 减半后的数值，然后开始执行拥塞避免算法（“加法增大”），使拥塞窗口缓慢地线性增大。 
![](http://img.blog.csdn.net/20170805220719694?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTcxMjE1MDE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

---
###5.套接字
####5.1 Socket 的交互流程  
![](http://om6ayrafu.bkt.clouddn.com/post/understand-tcp-udp/46872611EB6C0874FE9E4C290E8F3FE9.png)  

####5.2 Socket 接口
socket 是系统提供的接口，而操作系统大多数都是用 C/C++ 开发的，自然函数库也是 C/C++ 代码。   
####Socket 函数
该函数会返回一个套接字描述符（socket descriptor），但是该描述符仅是部分打开的，还不能用于读写。   
如何完成打开套接字的工作，取决于我们是客户端还是服务器。  

**函数原型**  
```c++
#include <sys/socket.h>

int socket(int domain, int type, int protocol);
```
**参数说明**  
**domain**: 协议域，决定了 socket 的地址类型，在通信中必须采用对应的地址。 常用的协议族有：AF_INET（ipv4地址与端口号的组合）、AF_INET6（ipv6地址与端口号的组合）、AF_LOCAL（绝对路径名作为地址）。 该值的常量定义在 sys/socket.h 文件中。  
**type**: 指定 socket 类型。 常用的类型有：SOCK_STREAM、SOCK_DGRAM、SOCK_RAW、SOCK_PACKET、SOCK_SEQPACKET等。 其中 SOCK_STREAM 表示提供面向连接的稳定数据传输，即 TCP 协议。 该值的常量定义在 sys/socket.h 文件中。  
**protocol**: 指定协议。 常用的协议有：IPPROTO_TCP（TCP协议）、IPPTOTO_UDP（UDP协议）、IPPROTO_SCTP（STCP协议）。 当值位 0 时，会自动选择 type 类型对应的默认协议。  

####bind函数
由服务端调用，把一个地址族中的特定地址和 socket 联系起来。
函数原型
```c++
#include <sys/socket.h>

int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
```

**参数说明**  
sockfd: 即 socket 描述字，由 socket() 函数创建。  
*addr： 一个 const struct sockaddr 指针，指向要绑定给 sockfd 的协议地址。   这个地址结构根据地址创建 socket 时的地址协议族不同而不同，例如 ipv4 对应   sockaddr_in，ipv6 对应 sockaddr_in6.   这几个结构体在使用的时候，都可以强制转换成 sockaddr。   下面是这几个结构体对应的所在的头文件：  

sockaddr： sys/socket.h
sockaddr_in： netinet/in.h
sockaddr_in6： netinet6/in.h

_in 后缀意义：互联网络(internet)的缩写，而不是输入(input)的缩写。

####listen 函数

服务器调用，将 socket 从一个主动套接字转化为一个监听套接字（listening socket）, 该套接字可以接收来自客户端的连接请求。   在默认情况下，操作系统内核会认为 socket   函数创建的描述符对应于主动套接字（acti ve socket）。  
**函数原型**  
```c++
#include <sys/socket.h>
int listen(int sockfd, int backlog);
```
**参数说明**
sockfd: 即 socket 描述字，由 socket() 函数创建。
backlog: 指定在请求队列中的最大请求数，进入的连接请求将在队列中等待 accept() 它们。
####connect 函数

由客户端调用，与目的服务器的套接字建立一个连接。  
**函数原型**
```c++
#include <sys/socket.h>
int connect(int clientfd, const struct sockaddr *addr, socklen_t addrlen);
```
**参数说明**

clientfd: 目的服务器的 socket 描述符  
*addr: 一个 const struct sockaddr 指针，包含了目的服务器 IP 和端口。  
addrlen： 协议地址的长度，如果是 ipv4 的 TCP 连接，一般为 sizeof(sockaddr_in); 
####accept 函数
服务器调用，等待来自客户端的连接请求。 当客户端连接，accept 函数会在 addr 中会填充上客户端的套接字地址，并且返回一个已连接描述符（connected descriptor），这个描述符可以用来利用 Unix I/O 函数与客户端通信。  
**函数原型**  
```c++
#indclude <sys/socket.h>
int accept(int listenfd, struct sockaddr *addr, int *addrlen);
```
**参数说明**
listenfd: 服务器的 socket 描述字，由 socket() 函数创建。  
*addr: 一个 const struct sockaddr 指针，用来存放提出连接请求客户端的主机的信息  
*addrlen: 协议地址的长度，如果是 ipv4 的 TCP 连接，一般为 sizeof(sockaddr_in)。  
####close 函数  

在数据传输完成之后，手动关闭连接。  
**函数原型**  

```c++
#include <sys/socket.h>
#include <unistd.h>
int close(int fd);
```
**参数说明**  
**fd**: 需要关闭的连接 socket 描述符

####网络 I/O 函数
当客户端和服务器建立连接后，可以使用网络 I/O 进行读写操作。 网络 I/O 操作有下面几组：
>
read()/write()  
recv()/send()  
readv()/writev()  
recvmsg()/sendmsg()  
recvfrom()/sendto()  

最常用的是 read()/write() 他们的原型是：
```c++
ssize_t read(int fd, void *buf, size_t count);
ssize_t write(int fd, const void *buf, size_t count);
```
鉴于该文是侧重于描述 socket 的工作原理，就不再详细描述这些函数了。